# Colors Application

To initialize application please follow these steps below
  - clone application from `git clone git@bitbucket.org:Tetsujinsan/colors.git` or
  - download zip form repository`https://bitbucket.org/Tetsujinsan/colors/src`
  - run `composer install` to install vendor PDO Driver 
  - change DB connection configuration in `App/Config/Config.php` to suite you postgres configuration
  
Config contains the following options:
```
  $config = array(
        'db_name' => 'postgres',
        'db_host' => '127.0.0.1',
        'db_port' => '5432',
        'db_user' => 'postgres',
        'db_password' => 'postgres',
    );
```

## Additional notes:
- no framework has been used to implement task requirements
- it is custom implementation using OOP and namespaces
- simple usage of bootstrap 4 and jquery has been used

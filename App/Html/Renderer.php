<?php

namespace App\Html;

class Renderer
{

    /**
     * Renderes total button
     * @return string HTML is returned
     */
    public function renderTotalButton()
    {
        $html = '';
        $html .= '<button type="button btn-default" class="js-get-total-votes">Get total votes</button>';
        return $html;
    }

    /**
     * This function renders color's table content
     * @param array $data Collection of data
     * @return string HTML is returned
     */
    public function renderTable($data)
    {
        $html = "";
        $html .= '<section class="table-container">
                    <div class="container">';
        $html .= '      <table class="table table-bordered">';
        $html .= '          <thead>
                                <tr>
                                    <th>Color</th>
                                    <th>Votes</th>
                                </tr>
                            </thead>
                            <tbody>';
        $html .= $this->renderTableBody($data);
        $html .= '          </tbody>';
        $html .= '          <tfoot>
                <tr>
                    <td>'.$this->renderTotalButton().'</td>
                    <td class="total-count"></td>
                </tr>
            </tfoot>';
        $html .= '      </table>';
        $html .= '  </div>
                </section>';
        return $html;
    }

    /**
     * Renders table body
     * @param array $data Collection of data to be represented as rows
     * @return string HTML is returned
     */
    private function renderTableBody($data)
    {
        $html = '';
        if (empty($data)) {
            $html .= '<tr>
                <td colspan="2">No data available</td>
            </tr>';
            return $html;
        }

        foreach($data as $row) {
            $html .= '<tr>
                    <td><a href="#" data-color-name="'.$row['name'].'" data-operation="get-votes-for-color" class="js-get-votes-for-color">'.$row['name'].'</a></td>
                    <td></td>
                </tr>';
        }

        return $html;
    }

    /**
     * This function returnes application HTML content. Content should be responsive
     * @return string HTML is returned
     */
    public function renderHtml()
    {
        $html = '';
        $html .= '<html lang="en">
            <head>
                <meta charset="utf-8" />
                <title>Colors Votes</title>
                <meta name="description" content="Votes for colors" />
                <meta name="robots" content="index,follow" />
                <meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.0, initial-scale=1.0" />
                <meta name="apple-mobile-web-app-capable" content="yes" />
                <link href="assets/css/animate.css" rel="stylesheet" />
                <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
                <link href="assets/css/style.css" rel="stylesheet" />
                <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
                <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
                <script type="text/javascript" src="assets/js/scripts.js"></script>
            </head>
            <body>'.$this->getContent().'</body>
            </html>';
        return $html;
    }

    /**
     * Sets content which is rendered in body section
     * @param string $content HTML string
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * Returnes content variable
     * @return string HTML should be returned
     */
    public function getContent()
    {
        return $this->content;
    }
}
<?php

namespace App\Processor;

use App\Processor\AjaxProcessorException;
use App\Response\JsonResponse;
use App\Entity\Votes;

class AjaxProcessor
{
    /**
     *
     * Ajax operation name
     * @var string
     */
    protected $operationName;

    public function __construct($operationName = null)
    {
        if (!empty($operationName)) {
            $this->operationName = $operationName;
        }
    }

    /**
     * Sets name of the ajax methos
     * @param string $operationName Name of the method to be invoked
     * @throws AjaxProcessorException An exception is thrown in case of errors
     */
    public function setOperationName(string $operationName)
    {
        if (empty($operationName)) {
            throw new AjaxProcessorException('Unable to set Ajax method name. Name is empty.');
        }

        $this->operationName = $operationName;
    }

    /**
     * Handles ajax call
     * @param array $requestData Collection of POST request data
     * @return string JSON response is returned
     * @throws AjaxProcessorException An Exception is thrown in case of errors
     */
    public function processCall($requestData = null)
    {
        if (empty($this->operationName)) {
            throw new AjaxProcessorException('Method name for Ajax call cannot be empty');
        }

        $methodName = $this->operationName;
        if (!method_exists($this, $this->operationName)) {
            throw new AjaxProcessorException('Given Ajax method does not exists');
        }

        return $this->$methodName($requestData);
    }

    /**
     * Handles receiving votes data for specific color
     * @param array $requestData Collection of POST data
     * @return JsonResponse Jdon response is returned
     */
    public function handleGetVotesForColor($requestData = null){
        $color = $requestData['color'];
        $votesEntity = new Votes();
        $colorVotes = $votesEntity->getVotes($color);

        return new JsonResponse([
           'success' => true,
            'votes' => $colorVotes,
        ]);
    }
}
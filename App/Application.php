<?php

namespace App;

use App\Config\Config;
use App\Html\Renderer;
use App\Response\JsonResponse;
use App\Request\Request;
use App\Entity\Color;
use Exception;

class Application
{
    protected $config;
    protected $renderer;
    protected $request;
    protected $response;

    public function __construct()
    {
        $this->config   = Config::getInstance();
        $this->renderer = new Renderer();
        $this->request  = new Request();
        $this->response = new JsonResponse();
    }

    /**
     * Handles Ajax and normal http call
     * @return mixed JSON response is returned in case of Ajax call, HTML is returned otherwise
     */
    public function run()
    {
        if ($this->request->detectAjaxCall()) {
            return $this->request->handleAjaxRequest();
        }

        return $this->process();
    }

    /**
     * Function renders main application
     * @return string HTML is returned
     */
    protected function renderAppContent()
    {
        $html        = "";
        $renderer    = new Renderer();
        $colorEntity = new Color();
        $data        = $colorEntity->findAll();

        $html .= $renderer->renderTable($data);
        return $html;
    }

    /**
     *
     * @return type
     */
    protected function process()
    {
        try {
            $this->renderer->setContent($this->renderAppContent());
            echo $this->renderer->renderHtml();
        } catch (Exception $e) {
            echo $this->response->returnResponse(array(
                'success' => false,
                'error' => $e->getMessage(),
            ));
        }
        return;
    }
}
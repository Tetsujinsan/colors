<?php

namespace App\Database;

use App\Config\Config;
use SeinopSys\PostgresDb;

class Database
{
    private static $instance;

    /**
     * Singleton for database isntance
     * @return PostgresDb instance is returned
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            $config         = Config::getInstance();
            $db             = new PostgresDb($config->get('db_name'), $config->get('db_host'), $config->get('db_user'), $config->get('db_password'));
            self::$instance = $db;
        }
        return self::$instance;
    }
}
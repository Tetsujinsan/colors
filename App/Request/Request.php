<?php

namespace App\Request;

use App\Response\JsonResponse;
use App\Request\RequestException;
use App\Processor\AjaxProcessor;
use App\Processor\AjaxProcessorException;

class Request
{
    protected $allowedMethods = array(
        'get-votes-for-color',
    );

    /**
     * detects if request contains ajax call
     * @return boolean TRUE is ajax, false otherwise
     */
    public function detectAjaxCall()
    {
        $requestedWith = filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH', FILTER_SANITIZE_STRING);

        if (!empty($requestedWith) && strtolower($requestedWith) == 'xmlhttprequest') {
            return true;
        }

        return false;
    }

    /**
     * returnes request data
     * @return array Collection of request data is returned
     */
    public function getRequestData()
    {
        $data = array();

        if (empty($_POST)) {
            return $data;
        }

        foreach ($_POST as $key => $value) {
            $data[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
        }

        return $data;
    }

    /**
     * Retrives operation name for request
     * @return string Operation name from Ajax request is returnd
     */
    private function getOperationName()
    {
        $operation = filter_input(INPUT_POST, 'operation', FILTER_SANITIZE_STRING);
        return $operation;
    }

    /**
     * Verifies allowed ajax method
     * @return boolean True is returned is methos is allowed, false otherwise
     * @throws RequestException An exception is thrown in case of errors
     */
    private function verifyMethod()
    {
        $operation = $this->getOperationName();
        if (!array_key_exists('operation', $_POST)) {
            throw new RequestException('Unrecognized operation type: '.$operation);
        }

        if (!array_key_exists($operation, array_flip($this->allowedMethods))) {
            throw new RequestException('Operation not allowed, terminated');
        }

        return true;
    }

    /**
     * Handles Ajax call
     * @return JsonResponse JSOn response is returned
     */
    public function handleAjaxRequest()
    {
        try {
            $this->verifyMethod();
        } catch (RequestException $e) {
            return new JsonResponse([
                'success' => false,
                'error' => $e->getMessage(),
            ]);
        }

        $operation = $this->getConvertedOperationName();
        try {
            $processor = new AjaxProcessor($operation);
            return $processor->processCall($this->getRequestData());
        } catch (AjaxProcessorException $e) {
            return new JsonResponse([
                'success' => false,
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Translates request operation name into camel-cased method from AjaxProcessor
     * @return string Converted methos name is returned
     */
    private function getConvertedOperationName()
    {
        $operationName = $this->getOperationName();
        $exp           = array_map('ucfirst', explode("-", $operationName));
        $str           = implode("", $exp);
        return 'handle'.$str;
    }
}
<?php

namespace App\Config;

use App\Config\ConfigException;

class Config
{
    private static $instance;

    /**
     * Config definition
     * @var array
     */
    private $config = array(
        'db_name' => 'postgres',
        'db_host' => '127.0.0.1',
        'db_port' => '5432',
        'db_user' => 'postgres',
        'db_password' => 'postgres',
    );

    private function __construct()
    {

    }

    private function __clone()
    {

    }

    /**
     * Returnes an instance of config, Singleton
     * @return Config an instance of config is returned
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Config();
        }
        return self::$instance;
    }

    /**
     * Returnes config variable
     * @param string $name name of thje variable
     * @return mixed value of the searched variable is returned
     * @throws ConfigException An exception is thrown in cse of errors
     */
    public function get($name)
    {
        if (!array_key_exists($name, $this->config)) {
            throw new ConfigException('Unrecognized config parameter named: '.$name);
        }

        return $this->config[$name];
    }
}
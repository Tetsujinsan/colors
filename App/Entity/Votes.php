<?php

namespace App\Entity;

use App\Database\Database;
use App\Exception\AppException;
use PDO;

class Votes
{
    protected $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
        $this->db->setPDOErrmode(PDO::ERRMODE_EXCEPTION)->getConnection();
        $this->db->getConnection();
    }

    /**
     * Returnes all rows form votes table
     * @return array Collection of rows is returned
     */
    public function findAll()
    {

        $query   = 'SELECT v.city, v.color, v.votes FROM votes v';
        $results = $this->db->query($query);
        return $results;
    }

    /**
     * Returnes votes for given color
     * @return array Collection of rows is returned
     */
    public function getVotes(string $colorName)
    {
        if (empty($colorName)) {
            throw new AppException('Unable to get Votes. Empty color name');
        }

        $query   = 'SELECT SUM(v.votes) as total FROM votes v WHERE color = :color';
        $results = $this->db->query($query, [':color' => $colorName,]);

        if(empty($results)) {
            return 0;
        }

        foreach($results as $collection) {
            if($collection['total'] === null) {
                return 0;
            }
            
            return $collection['total'];
        }        
    }
}
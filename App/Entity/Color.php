<?php

namespace App\Entity;

use App\Database\Database;
use PDO;

class Color
{
    protected $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
        $this->db->setPDOErrmode(PDO::ERRMODE_EXCEPTION)->getConnection();
        $this->db->getConnection();
    }

    /**
     * Returnes all rows form colors table
     * @return array Collection of rows is returned
     */
    public function findAll()    {

        $query = 'SELECT c.name FROM colors c';
        $results = $this->db->query($query);
        return $results;
    }
}
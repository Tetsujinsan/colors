<?php

namespace App\Response;

class JsonResponse
{

    public function __construct(array $data = null)
    {
        if(!empty($data)) {
            return $this->returnResponse($data);
        }
    }

    /**
     * Returnes JSON
     * @param array $data Raw response to be converted to JSON
     */
    public function returnResponse($data)
    {
        header('Content-Type: application/json');
        print json_encode($data);
        exit;
    }

}
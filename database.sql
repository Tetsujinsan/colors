-- Drop table

-- DROP TABLE public.votes;

CREATE TABLE public.votes (
	id serial NOT NULL,
	city varchar(150) NOT NULL,
	color varchar(25) NOT NULL,
	votes int4 NOT NULL
);

INSERT INTO public.votes (city,color,votes) VALUES 
('Anchorage','Blue',10000)
,('Anchorage','Yellow',15000)
,('Brooklyn','Blue',250000)
,('Detroit','Red',160000)
,('Selma','Yellow',15000)
,('Selma','Violet',5000)
,('Brooklyn','Red',100000)
;

-- Drop table

-- DROP TABLE public.colors;

CREATE TABLE public.colors (
	id serial NOT NULL,
	"name" text NULL,
	CONSTRAINT color_pkey PRIMARY KEY (id)
);

INSERT INTO public.colors ("name") VALUES 
('Red')
,('Orange')
,('Yellow')
,('Green')
,('Blue')
,('Indigo')
,('Violet')
;

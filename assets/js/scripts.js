var scripts = (function () {
    var results = {};
    var scope = {
        init: function () {
            $('body')
                    .on('click', '.js-get-total-votes', function (event) {
                        event.preventDefault();
                        scope._getTotalVotes($(this), event);
                    })
                    .on('click', '.js-get-votes-for-color', function (event) {
                        event.preventDefault();
                        scope._getVotesForColor($(this), event);
                    });
        },
        _getVotesForColor: function ($element, e) {
            var color = $element.data('colorName');
            var operation = $element.data('operation');
            var $td = $element.closest('td');

            $.ajax({
                type: 'post',
                url: window.location.href,
                data: {
                    color: color,
                    operation: operation
                },
                success: function (response) {
                    if (response.success === false) {
                        scope.renderAlert('danger', response.error, 5000);
                        return;
                    }
                    scope._updateTableCell($td.next(), response.votes);
                },
                error: function (response) {
                    scope.renderAlert('danger', 'General error occurred, please try again', 5000);
                    return;
                }
            });
        },
        _getTotalVotes: function ($element, e) {
            var $td = $element.closest('td');
            var $nextTd = $td.next();
            var total = 0;

            $("table tr td:nth-child(2)").each(function (i, e) {
                var $cell = $(e);
                var votes = 0;
                
                if($cell.hasClass('total-count')) {
                    return;
                }
                
                if ($cell.html().trim() !== '') {
                    votes = parseInt($cell.html().trim());
                }

                total = total + votes;
            });

            scope._updateTableCell($nextTd, total);
        },
        _updateTableCell: function ($td, content) {
            $td.html('');
            $td.html(content);
        },
        renderAlert: function (alertType, message, timeout) {
            var $html = $('<div class="alert alert-' + alertType + '" role="alert">' +
                    message +
                    '</div>');

            $html.prependTo($('body'));
            setTimeout(function () {
                $html.remove();
            }, timeout);
        }
    };


    for (var key in scope) {
        if (!scope.hasOwnProperty(key)) {
            continue;
        }

        if (key.charAt(0) === '_') {
            continue;
        }

        results[key] = scope[key];
    }

    return results;
})(window);

$(function () {
    scripts.init();
});
